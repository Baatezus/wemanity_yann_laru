const request = require('supertest')
const app = require('../server')
const helper = require('./helper')

describe('Contacts endpoints', () => {
    /**
     * Get All test
     */
    it('should return the contacts list', async () => {
        const res = await request(app).get('/api/contacts')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('data')
        expect(Array.isArray(res.body.data)).toBeTruthy()
    })



    /**
     * Search tests
     */
    it('should return matching contact', async () => {
        const contact = {
            first_name: "Dummy",
            last_name: "Data",
            phone_number: "+39 02 1234567", 
        }
        await helper.feedWith(contact).then(async () => {
            const res = await request(app).post('/api/search').send({filter: 'umm'})
            expect(res.statusCode).toEqual(200)
            for(const p in contact) {
                expect(res.body.data[0]).toHaveProperty(p, contact[p])
            }
        })
    })

    it('should return an empty array', async () => {
            const res = await request(app).post('/api/search').send({filter: 'azerty'})
            expect(res.body.data.length).toEqual(0)
    })

    it('should return matching contacts (2)', async () => {
        const contact = {
            first_name: "Data",
            last_name: "Dummy",
            phone_number: "+39 02 7564321", 
        }
        await helper.feedWith(contact).then(async () => {
            const res = await request(app).post('/api/search').send({filter: 'umm'})
            expect(res.body.data.length).toEqual(2)
        })
    })

    /**
     * create tests
     */
    it('should create a new entry', async () => {
        const contact = {
            first_name: "Dummy",
            last_name: "Data",
            phone_number: "+39 02 7654321", 
        }
        const res = await request(app)
          .post('/api/contacts')
          .send(contact) 
        expect(res.statusCode).toEqual(201)
        for(const p in contact) {
            expect(res.body.data).toHaveProperty(p, contact[p])
        }
    })

    it('should return an error 400, phone number already in use', async () => {
        const contact = {
            first_name: "Dummy",
            last_name: "Data",
            phone_number: "+39 02 1234567", 
        }
        const res = await request(app)
          .post('/api/contacts')
          .send(contact)  
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual({error: 'Phone number already in use'})
    })

    it('should return an error 400, wrong phone number format', async () => {
        const contact = {
            first_name: "Dummy",
            last_name: "Data",
            phone_number: "+39 023567", 
        }
        const res = await request(app)
          .post('/api/contacts')
          .send(contact)  
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual({error: 'Wrong phone number format'})
    })

    it('should return an error 400 due to missing parameters', async () => {
        const contact = {
            first_name: "Dummy",
            last_name: "Data"
        }
        const res = await request(app)
          .post('/api/contacts')
          .send(contact)  
        expect(res.statusCode).toEqual(400)
        expect(res.body).toEqual({error: 'bad parameters'})
    })

    /**
     * Update tests
     */
    it('should return an updated entry', async () => {
        const contact = {
            first_name: "IamNot",
            last_name: "Dummy!!!"
        }
        const res = await request(app)
          .put('/api/contacts/1')
          .send(contact) 
        expect(res.statusCode).toEqual(200)
        for(const p in contact) {
            expect(res.body.data).toHaveProperty(p, contact[p])
        }
    })

    it('should return a error 404', async () => {
        const contact = {
            first_name: "IamNot",
            last_name: "Dummy!!!"
        }
        const res = await request(app)
          .put('/api/contacts/10000')
          .send(contact) 
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({error: 'Ressource not found'})
    })

    /**
     * Delete test
     */
    it('should delete entry', async () => {
        const res = await request(app).delete('/api/contacts/1')
        expect(res.statusCode).toEqual(204)
    })

    it('should return a error 404', async () => {
        const res = await request(app).delete('/api/contacts/10000')
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({error: 'Ressource not found'})
    })

    /**
     * Find by id tests
     */

    it('should return the contact', async () => {
        const contact = {
            first_name: "Data",
            last_name: "Dummy",
            phone_number: "+39 02 7564321", 
        }
        const res = await request(app).get('/api/contacts/2')
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('data')
        for(const p in contact) {
            expect(res.body.data).toHaveProperty(p, contact[p])
        }
        
    })

    it('should return a error 404', async () => {
        const res = await request(app).get('/api/contacts/10000')
        expect(res.statusCode).toEqual(404)
        expect(res.body).toEqual({error: 'Ressource not found'})
    })
})
import React from 'react'
import './App.css'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Navbar from './components/navigation/NavBar'
import Contacts from './components/pages/Contacts'
import CreateContact from './components/pages/CreateContact'
import UpdateContact from './components/pages/UpdateContact'
import Alert from "./components/alerts/Alert";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Alert/>
        <Switch>
          <Route exact path='/' component={Contacts} />
          <Route path='/create' component={CreateContact} />
          <Route path='/update/:id' component={UpdateContact} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;

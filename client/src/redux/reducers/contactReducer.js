const initState = {
    contacts: [],
    error: null
} 

const contactReducer = (state = initState, action) => {
    switch (action.type) {
        case 'GET_ALL_CONTACTS':
            return {contacts: action.contacts.data.data}
        case 'GET_ALL_CONTACTS_ERROR':
            return {...state, error: action.error}
        case 'CREATE_CONTACT':
            return { ...state, contacts: state.contacts.concat([action.contact])}
        case 'CREATE_CONTACT_ERROR':
            return {...state, error: action.error}
        case 'UPDATE_CONTACT':
            return {...state, contacts: state.contacts.map(e => e.id === action.contact.id ? action.contact : e)}
        case 'UPDATE_CONTACT_ERROR':
            return {...state, error: action.error}
        case 'DELETE_CONTACT':
            return {...state, contacts: state.contacts.filter(e => e.id !== action.contactId)}
        case 'DELETE_CONTACT_ERROR':
            return {...state, error: action.error}
        case 'CLEAN_ERROR':
            return {...state, error: null}
        default:
            return state
    }
}

export default contactReducer
import axios from "axios"
import { BASE_URL } from '../../constants/urls.constants'

export const getAllContacts = () =>{
    return async dispatch => {
        const contacts = await axios.get(BASE_URL)
        .catch(error => dispatch({ type: 'GET_ALL_CONTACTS_ERROR', error }))
        dispatch({type: 'GET_ALL_CONTACTS', contacts})
    }
}

export const createContact = contact => {
    return async dispatch => {
        await axios.post(
            BASE_URL,
            contact
        ).catch(error => dispatch({ type: 'CREATE_ENTRY_ERROR', error}))
        dispatch({type: 'CREATE_CONTACT', contact})
    }
}

export const deleteContact = contactId => {
    return async dispatch => {
        await axios.delete(
            BASE_URL + contactId)
            .catch(error => dispatch({ type: 'DELETE_CONTACT_ERROR', error}))
        dispatch({type: 'DELETE_CONTACT', contactId})
    }
}

export const updateContact = contact => {
    return async dispatch => {
        await axios.put(
            BASE_URL + contact.id,
            contact
        ).catch(error => dispatch({ type: 'UPDATE_CONTACT_ERROR', error}))
        dispatch({type: 'UPDATE_CONTACT', contact})
    }
}


export const cleanError = () => {
    return async dispatch => dispatch({ type: 'CLEAN_ERROR' })
}

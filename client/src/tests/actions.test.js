import {describe, expect, it} from "@jest/globals";
import { createContact } from "../redux/actions/contactsActions"

describe('actions', () => {
    it('Create a new contact', () => {
        const contact = {
            first_name: 'Yann',
            last_name: 'LARU',
            phone_number: '0489 658 799'
        }
        const expectedAction = {
            type: 'CREATE_CONTACT',
            contact: {
                first_name: 'Yann',
                last_name: 'LARU',
                phone_number: '0489 658 799'
            }
        }
        expect(createContact(contact)).toEqual(expectedAction)
    })

    it('Should return an error 400', () => {
        const contact = {
            first_name: 'Yann',
            phone_number: '0489 658 799'
        }
        const expectedError = {
            status: 400,
            message: 'missing parameters'
        }
        expect(createContact(contact)).toEqual(expectedError)
    })
})
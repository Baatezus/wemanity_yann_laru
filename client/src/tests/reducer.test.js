import {describe, expect, it} from "@jest/globals";
import contactReducer from "../redux/reducers/contactReducer";

describe('Contact reducer', () => {
    it('should handle CREATE_CONTACT', () => {
        expect(
            contactReducer({contacts: []}, {
                type: 'CREATE_CONTACT',
                contact: {
                    first_name: 'Yann',
                    last_name: 'LARU',
                    phone_number: '0489 658 799'
                }
            })
        ).toEqual({
            contacts: [
                {
                    first_name: 'Yann',
                    last_name: 'LARU',
                    phone_number: '0489 658 799'
                }
            ]
        })
    })
})
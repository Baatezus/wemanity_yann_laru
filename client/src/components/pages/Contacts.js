import React, {useState, useEffect} from 'react'
import { connect } from 'react-redux'
import { getAllContacts, deleteContact } from '../../redux/actions/contactsActions'
import Contact from '../contacts/Contact'


const Contacts = ({contacts, getAllContacts, deleteContact}) => {

    useEffect(() => {
        getAllContacts()
    }, []);
    
    const [filter, setFilter] = useState('')

    const deleteAction = (e, i) => {
        e.preventDefault()
        deleteContact(i)
    }

    const filterContacts = e => setFilter(e.target.value.toLowerCase()) 
    const isMatchingFirstName = contact => contact.first_name.toLowerCase().indexOf(filter) >= 0
    const isMatchingLastName = contact => contact.last_name.toLowerCase().indexOf(filter) >= 0
    const isMatchingPhoneNumber = contact => contact.phone_number.toLowerCase().indexOf(filter) >= 0
    const isMatchingFilter = c => isMatchingFirstName(c) || isMatchingLastName(c) || isMatchingPhoneNumber(c)

    return (
        <div className="container">
            <h4>Contacts list</h4>
            <div className="input-field">
                <label htmlFor="filter">Filter list</label>
                <input id="filter" type="text" onChange={filterContacts} />
            </div>
            <div className="row">
            {
                contacts.length > 0 && contacts.filter(
                    c => filter.length > 0 ? isMatchingFilter(c) : c  
                )
                .map( contact => {
                    return (
                        <Contact contact={contact} deleteAction={deleteAction} />
                    )
                }) 
                || <p style={{textAlign: 'center'}}>Loading data...</p>
               
            }
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {
    return { contacts: state.contact.contacts }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        getAllContacts: () => dispatch( getAllContacts() ) ,
        deleteContact: contactId => dispatch( deleteContact( contactId ) )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Contacts)
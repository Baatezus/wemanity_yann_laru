import React, { useState } from 'react'
import { connect } from 'react-redux'
import { createContact } from '../../redux/actions/contactsActions'


const CreateContact = ({createContact, ...props}) => {

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: ''
    })

    const submit = (e) => {
        e.preventDefault()
        createContact(formData)
        props.history.push('/')
    }

    const handleChange = (e) => {
        setFormData({
            ...formData, 
            [e.target.id]: e.target.value
        })
    }

    return(
        <div className="container">
            <h4>Create new contact</h4>
            <form onSubmit={submit} className="white">
                <div className="input-field">
                    <label htmlFor="first_name">First Name</label>
                    <input required value={formData.first_name} type="text" id="first_name" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="lastName">Last Name</label>
                    <input required value={formData.last_name} type="text" id="last_name" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label htmlFor="phone_number">Phone number (format: "+xx xx xxxxxx...")</label>
                    <input pattern="^\+[0-9]{2}[0-9]{2}[0-9]{6,10}" value={formData.phone_number} type="text" id="phone_number" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0">Create</button>
                </div>
            </form>
        </div>
    )
}

const mapDispatchToProps = (dispatch) => {
    return { createContact: contact => dispatch( createContact( contact ) ) }
}

export default connect(null, mapDispatchToProps)(CreateContact)
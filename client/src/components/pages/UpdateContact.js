import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { updateContact } from '../../redux/actions/contactsActions'
import { useParams } from "react-router-dom";


const UpdateContact = ({contacts, updateContact, ...props}) => {

    if(contacts.length < 1) props.history.push('/')

    const { id } = useParams()

    const [formData, setFormData] = useState({
        first_name: '',
        last_name: '',
        phone_number: ''
    })

    useEffect(() => {
        setFormData(contacts.find(e => e.id === parseInt(id)))
    }, [])

    const submit = (e) => {
        e.preventDefault()
        updateContact(formData)
        props.history.push('/')
    }

    const handleChange = (e) => {
        setFormData({
            ...formData, 
            [e.target.id]: e.target.value
        })
    }

    return(
        <div className="container">
            <h4>Update contact</h4>
            {
                contacts.length > 0 && 
            <form onSubmit={submit} className="white">
                <div className="input-field">
                    <label className="active" htmlFor="first_name">First Name</label>
                    <input required value={formData.first_name} type="text" id="first_name" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label className="active" htmlFor="lastName">Last Name</label>
                    <input required value={formData.last_name} type="text" id="last_name" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <label className="active" htmlFor="phone_number">Phone number (format: "+xx xx xxxxxx...")</label>
                    <input pattern="^\+[0-9]{2}[0-9]{2}[0-9]{6,10}"value={formData.phone_number} type="text" id="phone_number" onChange={handleChange} />
                </div>
                <div className="input-field">
                    <button className="btn pink lighten-1 z-depth-0">Update</button>
                </div>
            </form>
            }
        </div>
    )
}

const mapStateToProps = (state) => {
    return { contacts: state.contact.contacts }
}

const mapDispatchToProps = (dispatch) => {
    return { 
        updateContact: contact => dispatch( updateContact( contact ) ) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateContact)
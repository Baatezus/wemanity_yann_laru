import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { ToastContainer, toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cleanError } from "../../redux/actions/contactsActions"

const Alert = ({error, cleanError}) => {
    useEffect(() => {
        if (error) toast(error, {onclose: cleanError()})
    }, [error])

    return (<ToastContainer />)
}

const mapStateToProps = state => {
    return {
        error: state.contacts.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        cleanError: () => dispatch( cleanError() )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Alert)


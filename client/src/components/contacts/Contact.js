import React from 'react'
import { Link } from 'react-router-dom'

const Contact = ({contact, deleteAction}) => {
    const deleteContact = (e, contactId) => deleteAction(e, contactId)

    return (
        <div className="col s4">
            <div className="card blue-grey darken-1">
                <div className="card-content white-text">
                <span className="card-title">{`${contact.first_name} ${contact.last_name}`}</span>
                    <h5>{contact.phone_number}</h5>
                </div>
                <div className="card-action">
                <Link to={`/update/${contact.id}`}>EDIT</Link>
                <Link to={'/'} onClick={(e) => deleteContact(e, contact.id)}>DELETE</Link>
                </div>
            </div>
        </div>
    )
}

export default Contact

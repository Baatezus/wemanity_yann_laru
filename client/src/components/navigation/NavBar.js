import React from 'react'
import { Link, NavLink, withRouter } from 'react-router-dom'

const NavBar = () => {
    return (
        <nav className="nav-wrapper grey darken-3">
            <div className="container">
                <Link className="brand-logo" to='/'>Wemanity phonebook</Link>
                <ul className="right">
                    <li><NavLink to="/">Contacts</NavLink></li>
                    <li><NavLink to="/create">New contact</NavLink></li>
                </ul>
            </div>
        </nav>
    )
}

export default withRouter(NavBar)
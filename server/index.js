const express = require('express')
const routes = require('../routes')
const bodyParser = require('body-parser')
const cors = require('cors')

const server = express()

server.use(bodyParser.urlencoded({ extended: true }))
server.use(express.json())
server.use(cors())
server.options('*', cors());
server.use('/api', routes)

module.exports = server
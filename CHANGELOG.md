# CHANGELOG

## Sprint: Eva-00

### BACKEND
#### Added
- Database configuration and model
- Fake data seeder
- Get all contacts
- Search contacts
- Create contact
- Update contact
- Delete contact
- Find contact by id



### CLIENT
#### Added
- Home page
- Create contact page
- Delete contact
- Update contact
- Filter in home page

#### Modified
- Components refactored
'use strict';

module.exports = {
  up: (queryInterface, Sequelize)=> queryInterface.bulkInsert(
    'Contacts',
    [
      {
        first_name: 'Jane',
        last_name: 'Doe',
        phone_number: '+39 02 1234567',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        first_name: 'John',
        last_name: 'Doe',
        phone_number: '+40 03 7654321',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        first_name: 'David',
        last_name: 'Bowie',
        phone_number: '+41 05 6665550',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        first_name: 'Nick',
        last_name: 'Cave',
        phone_number: '+42 03 9531456',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        first_name: 'Polly Jean',
        last_name: 'Harvey',
        phone_number: '+39 45 1237779',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
    {},
  ),

  down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Users', null, {}),
}

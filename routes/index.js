const { Router } = require('express')
const controller = require('../controllers')

const router = Router()

router.get('/', (req, res) => res.status(200).json({data: "welcome"}))
router.get('/contacts', controller.getAllContacts)
router.post('/search', controller.search)
router.post('/contacts', controller.createContact)
router.put('/contacts/:contactId', controller.updateContact)
router.delete('/contacts/:contactId', controller.deleteContact)
router.get('/contacts/:contactId', controller.findContactById)

module.exports = router
# WEMANITY PHONEBOOK KATA



## GETTING STARTED

### Clone this repository

```git clone https://Baatezus@bitbucket.org/Baatezus/wemanity_yann_laru.git```

### You will need PostGreSQL to run the project

- Create two databases with the respective name of "wemanity_dev" and "wemanity_test"
- Find the "``.env``" file in the root folder and change the two occurances of "``<PostGreSQL user>``" by your PostGreSQL user name and the two occurances of "``<PostGreSQL password>``" by your PostGreSQL password.

### Installing dependencies & feeding database

#### Backend: 
At the project root run the folowings commands: 
- Installing dependencies  
``npm i``

- Create a migration    
``sequelize db:migrate``

- Feed the database   
``sequelize db:seed:all``

#### Front end:
 - Get to the client folder   
 ``cd client``

 - Installing dependencies  
``npm i``

## TESTING

### Back end tests
Run the folowing command at the project root    
``npm test``

### Front end test
Run the folowing command in the client folder
``npm run test``

## LAUNCH THE APP

#### Backend
Run the folowing command at the project root    
``nodemon``

#### Frontend
 - Get to the client folder    
 ``cd client``

 - Launch the app    
 ``npm start``

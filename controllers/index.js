const models = require('../database/models')
const Sequelize = require('sequelize')
const Op = Sequelize.Op

const getAllContacts = async (req, res) => {
    try {
        const contacts = await models.Contact.findAll()
        res.status(200).json({data: contacts})
    } catch (error) {
        return res.status(500).send(error.message)
    }
}

const search = async (req, res) => {
    try {
        const { filter } = req.body
        const contacts = await models.Contact.findAll({
            where: {
                [Op.or]: [
                    {
                        first_name: {
                            [Op.iLike]: `%${filter}%`
                        }
                    },
                    {
                        last_name: {
                            [Op.iLike]: `%${filter}%`
                        }
                    },
                    {
                        phone_number: {
                            [Op.iLike]: `%${filter}%`
                        }
                    },
                ]
            }
        })
        res.status(200).json({data: contacts})
    } catch(error) {
        return res.status(500).send(error.message)
    }
} 

const createContact = async (req, res) => {
    try {
        const {first_name, last_name, phone_number} = req.body
        if(!first_name || !last_name || !phone_number)
            return res.status(400).json({error: 'missing parameters'})

        const regex = /\+[0-9]{2} [0-9]{2} [0-9]{6,}$/
        if(!regex.test(phone_number))
            return res.status(400).json({error: 'Wrong phone number format'})

        const existingEntry = await models.Contact.findOne({where: {phone_number: phone_number}})
        if(existingEntry)
            return res.status(400).json({error: 'Phone number already in use'})

        const contact = await models.Contact.create(req.body)
        return res.status(201).json({ data: contact })
    } catch (error) {
        return res.status(500).json({error: error.message})
    }
}

const updateContact = async (req, res) => {
    try {
        const { contactId } = req.params
        const [ updated ] = await models.Contact.update(req.body, { where: { id: contactId }})

        if (updated) {
            const updatedContact = await models.Contact.findOne({ where: { id: contactId } })
            return res.status(200).json({ data: updatedContact })
        }

        return res.status(404).json({error: 'Ressource not found'})
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

const deleteContact = async (req, res) => {
    try {
        const { contactId } = req.params;
        const deleted = await models.Contact.destroy({where: { id: contactId }})
        if (deleted) {
            return res.status(204).send({ data: "Contact has been deleted" })
        }

        return res.status(404).json({error: 'Ressource not found'})
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

const findContactById = async (req, res) => {
    try {
      const { contactId } = req.params;
      const contact = await models.Contact.findOne({ where: { id: contactId }})

      if (contact) {
        return res.status(200).json({ data : contact });
      }
      return res.status(404).json({error: 'Ressource not found'})
    } catch (error) {
      return res.status(500).send(error.message);
    }
  }

module.exports = {
  getAllContacts,
  search,
  createContact,
  updateContact,
  deleteContact,
  findContactById
}